use std::mem::MaybeUninit;

use async_pidfd::{AsyncPidFd, ExitInfo, PidFd};
use libc::{pid_t, syscall, SYS_waitid, P_PID, WEXITED, WNOWAIT};
use tokio::task::spawn_blocking;

fn wait(pid: pid_t) -> std::io::Result<ExitInfo> {
    let mut siginfo = MaybeUninit::uninit();
    let mut rusage = MaybeUninit::uninit();
    unsafe {
        let ret = syscall(
            SYS_waitid,
            P_PID,
            pid,
            siginfo.as_mut_ptr(),
            WEXITED | WNOWAIT,
            rusage.as_mut_ptr(),
        );
        if ret == -1 {
            return Err(std::io::Error::last_os_error());
        }
        Ok(ExitInfo {
            siginfo: siginfo.assume_init(),
            rusage: rusage.assume_init(),
        })
    }
}

pub enum SyncPidWait {
    Pid(pid_t),
    Fd(PidFd),
}

impl SyncPidWait {
    pub fn new(pid: pid_t) -> Self {
        match PidFd::from_pid(pid) {
            Ok(fd) => Self::Fd(fd),
            Err(_) => Self::Pid(pid),
        }
    }

    pub fn wait(&self) -> std::io::Result<ExitInfo> {
        match self {
            Self::Fd(fd) => fd.wait(),
            Self::Pid(pid) => wait(*pid),
        }
    }
}

pub enum AsyncPidWait {
    Pid(pid_t),
    Fd(AsyncPidFd),
}

impl AsyncPidWait {
    pub fn new(pid: pid_t) -> Self {
        match AsyncPidFd::from_pid(pid) {
            Ok(fd) => Self::Fd(fd),
            Err(_) => Self::Pid(pid),
        }
    }

    pub async fn wait(&self) -> std::io::Result<ExitInfo> {
        match self {
            Self::Fd(fd) => fd.wait().await,
            Self::Pid(pid) => {
                let pid = *pid;
                spawn_blocking(move || wait(pid))
                    .await
                    .expect("Thread should be neither cancelled or panic")
            }
        }
    }
}
