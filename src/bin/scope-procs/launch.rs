use std::{
    borrow::Cow,
    convert::TryInto,
    fs::OpenOptions,
    io::{stdout, Write},
    os::unix::{ffi::OsStrExt, fs::OpenOptionsExt},
    path::PathBuf,
    sync::Arc,
};

use futures_util::stream::TryStreamExt;
use libc::O_NDELAY;
use nix::{errno::Errno, sys::stat::Mode, unistd::mkfifo};
use serde::{Deserialize, Serialize};
use thiserror::Error;
use tokio::{io::unix::AsyncFd, runtime};
use tracing::{debug, error, info};
use zbus::azync::ProxyDefault;

use cli_test_toolkit::{
    background::background, scope_procs::AsyncScopeProcsProxy,
};

#[derive(Clone, Debug, Deserialize, Error, Serialize)]
pub(crate) enum SetupError {
    #[error("log open failed: {0:?}")]
    LogOpenFailed(String),
    #[error("setting subscriber failed: {0:?}")]
    SubscriberAssignFailed(String),
    #[error("creating fifo failed: {0:?}")]
    MkfifoFailed(String),
    #[error("opening fifo failed: {0:?}")]
    FifoOpenFailed(String),
    #[error("creating tokio runtime failed: {0:?}")]
    CreateTokioRuntimeFailed(String),
    #[error("processing DBus session bus address failed: {0:?}")]
    DBusGetAddressFailed(String),
    #[error("connecting to DBus failed: {0:?}")]
    DBusConnectFailed(String),
    #[error("creating DBus proxy failed: {0:?}")]
    DBusCreateProxyFailed(String),
    #[error("parsing well-known bus name failed: {0:?}")]
    DBusParseWellKnownNameFailed(String),
    #[error("requesting bus name failed: {0:?}")]
    DBusRequestNameFailed(String),
}

fn dispatch_msg<'a, 'b, 'c>(
    msg_header: &zbus::MessageHeader,
    path: &'a str,
    interface: &'b str,
    member: &'c str,
) -> Result<(), (&'static str, Cow<'static, str>)> {
    let msg_path = match msg_header.path().ok().flatten() {
        Some(path) => path,
        None => {
            info!("Message missing path");
            return Err((
                "org.freedesktop.DBus.Error.Failed",
                "Missing object path".into(),
            ));
        }
    };
    let msg_interface = match msg_header.interface().ok().flatten() {
        Some(interface) => interface,
        None => {
            info!("Message missing interface");
            return Err((
                "org.freedesktop.DBus.Error.Failed",
                "Missing interface".into(),
            ));
        }
    };
    let msg_member = match msg_header.member().ok().flatten() {
        Some(member) => member,
        None => {
            info!("Message missing member");
            return Err((
                "org.freedesktop.DBus.Error.Failed",
                "Missing method".into(),
            ));
        }
    };

    if msg_path != path {
        info!("Message for unknown object {}", msg_path);
        return Err((
            "org.freedesktop.DBus.Error.UnknownObject",
            format!("Unknown object '{}'", msg_path).into(),
        ));
    };
    if msg_interface != interface {
        info!("Message for unknown interface {}", msg_interface);
        return Err((
            "org.freedesktop.DBus.Error.UnknownInteface",
            format!("Unknown interface '{}'", msg_interface).into(),
        ));
    };
    if msg_member != member {
        info!("Message for unknown member {}", msg_member);
        return Err((
            "org.freedesktop.DBus.Error.UnknownMethod",
            format!("Unknown method '{}'", msg_member).into(),
        ));
    };
    Ok(())
}

pub(crate) fn launch_watcher(
    pid_file: PathBuf,
    fifo_file: PathBuf,
    serve_bus: Option<String>,
    log_file: Option<PathBuf>,
) -> anyhow::Result<()> {
    let setup = || {
        if let Some(log_file) = log_file {
            let f = OpenOptions::new()
                .write(true)
                .create(true)
                .truncate(true)
                .open(log_file)
                .map_err(|e| SetupError::LogOpenFailed(format!("{}", e)))?;
            let subscriber = tracing_subscriber::fmt::Subscriber::builder()
                .with_max_level(tracing::Level::DEBUG)
                .with_writer(Arc::new(f))
                .finish();
            tracing::subscriber::set_global_default(subscriber).map_err(
                |e| SetupError::SubscriberAssignFailed(format!("{}", e)),
            )?;
        }
        info!("Logging begins");

        match mkfifo(&fifo_file, Mode::S_IRUSR | Mode::S_IWUSR) {
            Ok(()) | Err(Errno::EEXIST) => Ok(()),
            Err(e) => Err(e),
        }
        .map_err(|e| SetupError::MkfifoFailed(format!("{}", e)))?;

        let pipefd = OpenOptions::new()
            .read(true)
            .custom_flags(O_NDELAY)
            .open(&fifo_file)
            .map_err(|e| SetupError::FifoOpenFailed(format!("{}", e)))?;

        // TODO: do cgroup setup.
        // TODO: install SIGTERM handler that kills cgroup

        let runtime = runtime::Builder::new_current_thread()
            .enable_io()
            .build()
            .map_err(|e| {
                SetupError::CreateTokioRuntimeFailed(format!("{}", e))
            })?;
        let (dbus, ticker) = runtime.block_on(async {
            let dbus = match serve_bus {
                Some(addr) => {
                    zbus::azync::ConnectionBuilder::address(addr.as_ref())
                }
                None => zbus::azync::ConnectionBuilder::session(),
            }
            .map_err(|e| SetupError::DBusGetAddressFailed(format!("{}", e)))?
            .build()
            .await
            .map_err(|e| SetupError::DBusConnectFailed(format!("{}", e)))?;

            // TODO: Tick executor until done with zbus connect
            let ticker = {
                let dbus = dbus.clone();
                async move {
                    loop {
                        dbus.executor().tick().await;
                    }
                }
            };

            let register = async {
                zbus::fdo::AsyncDBusProxy::new(&dbus)
                    .await
                    .map_err(|e| {
                        SetupError::DBusCreateProxyFailed(format!("{}", e))
                    })?
                    .request_name(
                        AsyncScopeProcsProxy::DESTINATION.try_into().map_err(
                            |e| {
                                SetupError::DBusParseWellKnownNameFailed(
                                    format!("{}", e),
                                )
                            },
                        )?,
                        zbus::fdo::RequestNameFlags::ReplaceExisting.into(),
                    )
                    .await
                    .map_err(|e| {
                        SetupError::DBusRequestNameFailed(format!("{}", e))
                    })?;
                Ok(())
            };

            let mut ticker = Box::pin(ticker);
            let res = tokio::select! {
                _ = &mut ticker => unreachable!(),
                res = register => res,
            };
            res?;

            Ok((dbus, ticker))
        })?;

        Ok::<_, SetupError>((fifo_file, (pipefd, runtime, dbus, ticker)))
    };
    let ready = |res: Result<PathBuf, _>| match res {
        Err(e) => eprintln!("{}", e),
        Ok(fifo_file) => {
            stdout()
                .write_all(fifo_file.as_os_str().as_bytes())
                .expect("should be able to write to stdout");
        }
    };
    let (pipefd, runtime, mut dbus, ticker) =
        background(setup, ready, Some(pid_file))?;

    let handle_messages = async {
        debug!("handling messages");
        debug!("Entering message loop");
        loop {
            debug!("Attempting to receive message");
            let msg = match dbus.try_next().await? {
                Some(msg) => msg,
                None => break,
            };
            debug!("Got message {msg}", msg = msg);
            let msg_header = msg.header()?;

            match msg_header.message_type()? {
                zbus::MessageType::MethodCall => {
                    match dispatch_msg(
                        &msg_header,
                        AsyncScopeProcsProxy::PATH,
                        AsyncScopeProcsProxy::INTERFACE,
                        "Shutdown",
                    ) {
                        Ok(()) => (),
                        Err((kind, response)) => {
                            dbus.reply_error(&msg, kind, &response).await?;
                            continue;
                        }
                    };

                    debug!("Got shutdown message");
                    dbus.reply(&msg, &()).await?;
                    break;
                }
                _ => continue,
            }
        }
        anyhow::Result::Ok(())
    };

    let handle_exit = async {
        debug!("handling exit");
        let pipefd =
            AsyncFd::with_interest(pipefd, tokio::io::Interest::READABLE)?;
        debug!("Awaiting pipefd readable");
        let _guard = pipefd.readable().await?;
        anyhow::Result::<_, anyhow::Error>::Ok(())
    };
    debug!("entering main loop");
    runtime
        .block_on(async {
            tokio::select! {
                _ = ticker => unreachable!(),
                res = handle_exit => res,
                res = handle_messages => res,
            }
        })
        .map_err(|error| {
            error!(%error);
            error
        })?;
    Ok(())
}
