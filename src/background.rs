use core::fmt::Debug;
use std::{path::Path, process::exit};

use daemonize::{Daemonize, DaemonizeError, ForkSafe};
use ipc_channel::ipc::{self, IpcSender};
use serde::{Deserialize, Serialize};
use thiserror::Error;

// Wrapper around IpcSender to pass it through Daemonize
// that is dropped in the parent and passed to the pre-privilege-drop hook.
struct ForkSafeIpcSender<T: Serialize>(IpcSender<T>);

unsafe impl<T> ForkSafe for ForkSafeIpcSender<T>
where
    T: Serialize,
{
    type Parent = ();
    type Child = IpcSender<T>;
    unsafe fn into_parent(self) -> Self::Parent {}
    unsafe fn into_child(self) -> Self::Child {
        self.0
    }
}

#[derive(Debug, Error)]
pub enum BackgroundError<E>
where
    E: 'static + Debug + std::error::Error,
{
    #[error("Creating IPC channel failed: {0:?}")]
    IpcChannelCreateError(#[source] std::io::Error),
    #[error("Setup failed: {0:?}")]
    SetupError(#[source] E),
    #[error("Daemonize failed: {0:?}")]
    DaemonizeError(#[source] DaemonizeError),
}

#[derive(Debug, Error)]
pub enum ReadyError<E>
where
    E: 'static + Debug + std::error::Error,
{
    #[error("Waiting for background process start failed: {0:?}")]
    CommunicateError(ipc::IpcError),
    #[error("Background process setup failed: {0:?}")]
    SetupError(#[source] E),
}

// TODO: Investigate how awkward a builder pattern would be
#[derive(Clone, Debug, Deserialize, Error, Serialize)]
/// Convenience define for when the caller's setup does not error
/// because ! is not stable
pub enum NoSetupError {}

/// Convenience default setup function for when the caller does no setup
pub fn default_setup() -> Result<((), ()), NoSetupError> {
    Ok(((), ()))
}

/// Convenience default ready function for when the caller only needs to report
/// the background process's errors and not communicate any state
pub fn default_ready<E>(res: Result<(), ReadyError<E>>)
where
    E: Clone
        + Debug
        + std::error::Error
        + for<'de> Deserialize<'de>
        + Serialize,
{
    if let Err(e) = res {
        eprintln!("{}", e);
    }
}

pub fn background<S, R, O, T, E>(
    setup: S,
    ready: R,
    pid_file: Option<impl AsRef<Path>>,
) -> Result<T, BackgroundError<E>>
where
    S: FnOnce() -> Result<(O, T), E>,
    R: FnOnce(Result<O, ReadyError<E>>),
    E: Clone
        + Debug
        + std::error::Error
        + for<'de> Deserialize<'de>
        + Serialize,
    O: for<'de> Deserialize<'de> + Serialize,
{
    let (tx, rx) = ipc::channel::<Result<O, E>>()
        .map_err(BackgroundError::IpcChannelCreateError)?;
    let mut daemonize = Daemonize::new().working_directory(".");
    if let Some(pid_file) = pid_file {
        daemonize = daemonize.pid_file(pid_file);
    }
    let res: Result<_, DaemonizeError> = daemonize
        .value(ForkSafeIpcSender(tx))
        .exit_action(|_| match rx.recv() {
            Err(e) => {
                ready(Err(ReadyError::CommunicateError(e)));
                exit(1);
            }
            Ok(Err(e)) => {
                ready(Err(ReadyError::SetupError(e)));
                exit(1);
            }
            Ok(Ok(msg)) => ready(Ok(msg)),
        })
        .privileged_action(|_tx| (_tx, setup()))
        .start();
    let (tx, setup_result) = res.map_err(BackgroundError::DaemonizeError)?;
    let (to_launcher, to_background) = match setup_result {
        Ok(success) => success,
        Err(e) => {
            tx.send(Err(e.clone()))
                .expect("grandparent process should wait for this response");
            return Err(BackgroundError::SetupError(e));
        }
    };
    tx.send(Ok(to_launcher))
        .expect("grandparent process should wait for this response");
    Ok(to_background)
}
