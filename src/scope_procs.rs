use zbus::dbus_proxy;

#[dbus_proxy(
    default_service = "name.maw.richard.ScopeProcs",
    interface = "name.maw.richard.ScopeProcs",
    default_path = "/name/maw/richard/ScopeProcs"
)]
trait ScopeProcs {
    fn shutdown(&self) -> zbus::Result<()>;
}
