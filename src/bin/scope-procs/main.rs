use std::{fs::read_link, path::PathBuf};

use anyhow::{anyhow, Context};
use libc::pid_t;
use structopt::StructOpt;
use thiserror::Error;
use xdg::BaseDirectories;

mod kill;
mod launch;
use kill::kill_watcher;
use launch::launch_watcher;
use tracing::error;

#[derive(Debug, StructOpt)]
#[structopt(name = env!("CARGO_BIN_NAME"))]
struct Args {
    #[structopt(short, long)]
    kill: bool,
    #[structopt(short, long)]
    log_file: Option<PathBuf>,
    #[structopt(short, long)]
    /// Parent process ID, defaults to the result of calling getppid(),
    /// used to infer the path to the pidfile for creation and --kill.
    /// The option exists to permit this to work when it is not a direct
    /// subprocess e.g. when run with strace by passing --ppid=$$.
    /// Technically any PID may be used, but it should only be a parent
    /// to provide an ID that is unique for the lifetime of the process.
    ppid: Option<pid_t>,
    #[structopt(short, long)]
    serve_bus: Option<String>,
}

#[derive(Debug, Error)]
enum GetNsidError {
    #[error("readlink failed: {source:?}")]
    ReadLinkError {
        #[from]
        source: std::io::Error,
    },
    #[error("namespace link not decodable")]
    LinkUndecodable,
    #[error("namespace link does not begin pid:[")]
    LinkInvalidPrefix,
    #[error("namespace link does not end ]")]
    LinkInvalidSuffix,
    #[error("namespace link number unparseable: {source:?}")]
    LinkUnparseable {
        #[from]
        source: std::num::ParseIntError,
    },
}

fn get_nsid() -> Result<u64, GetNsidError> {
    let p = read_link("/proc/self/ns/pid")?;
    let s = p.to_str().ok_or(GetNsidError::LinkUndecodable)?;
    let s = s
        .strip_prefix("pid:[")
        .ok_or(GetNsidError::LinkInvalidPrefix)?;
    let s = s.strip_suffix(']').ok_or(GetNsidError::LinkInvalidSuffix)?;
    Ok(s.parse()?)
}

#[paw::main]
fn main(args: Args) -> anyhow::Result<()> {
    let dirs = BaseDirectories::with_prefix(env!("CARGO_BIN_NAME"))
        .context("Failed to load xdg base directories")?;
    // Safety: Trust me on this, you can't break anything just getting the ppid.
    let ppid: pid_t = args.ppid.unwrap_or_else(|| unsafe { libc::getppid() });
    if ppid == 1 {
        return Err(anyhow!("Can't watch for processes reparented to init"));
    }
    let nsid: u64 = get_nsid().context("Failed to get PID namespace ID")?;
    let pid_file = dirs
        .place_runtime_file(format!("{}.{}.pid", nsid, ppid))
        .context("Failed to place pid file")?;
    let fifo_file = dirs
        .place_runtime_file(format!("{}.{}.fifo", nsid, ppid))
        .context("Failed to place fifo")?;
    if args.kill {
        kill_watcher(pid_file).context("Failed to kill watcher")?;
        Ok(())
    } else {
        launch_watcher(pid_file, fifo_file, args.serve_bus, args.log_file)
            .context("Failed to launch watcher")
            .map_err(|error| {
                error!(%error);
                error
            })?;
        Ok(())
    }
}
