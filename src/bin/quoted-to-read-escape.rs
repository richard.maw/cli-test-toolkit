use std::{array, iter};

use shell_words::split;
use structopt::StructOpt;

#[derive(Debug, StructOpt)]
#[structopt(name = env!("CARGO_BIN_NAME"))]
/// Escape text such that it may be read back with the read builtin.
///
/// Transforms from quoted words escape form (e.g. '"foo bar" baz')
/// to the \ escaped form preferred by the read builtin (e.g. 'foo\ bar baz').
///
/// It is intended to parse pre-escaped shell words e.g. from user input.
/// To convert arguments to escaped shell words use printf's %q format specifier
/// or the @Q variable expansion modifier.
///
/// Arguments are concatenated for convenience of making
/// ${*@Q}, "${*@Q}" and "${@@Q}" equivalent.
struct Args {
    /// Embedded separators to \ escape
    #[structopt(short = "i", long, env = "IFS", default_value = " \t\n")]
    input_separator: String,
    /// Character to separate output with
    #[structopt(short = "o", long, default_value = " ")]
    output_separator: String,
    /// Delimiter to terminate output with
    #[structopt(short = "d", long, default_value = "\n")]
    delimiter: String,
    /// Pre-quote-escaped string e.g. '"foo bar" baz'
    words: Vec<String>,
}

#[paw::main]
fn main(args: Args) -> anyhow::Result<()> {
    // TODO: If intersperse stabilises use instead of join.
    let s: String = args.words.join(" ");
    let words = split(&s)?;
    let s: String = words
        .into_iter()
        .map(|s| {
            s.chars()
                .map::<Box<dyn Iterator<Item = char>>, _>(|c| {
                    if args.input_separator.contains(c) || c == '\\' {
                        Box::new(array::IntoIter::new(['\\', c]))
                    } else {
                        Box::new(iter::once(c))
                    }
                })
                .flatten()
                .collect()
        })
        .collect::<Vec<String>>()
        .join(&args.output_separator);
    print!("{}{}", s, args.delimiter);
    Ok(())
}
