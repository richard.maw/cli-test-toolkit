#!/bin/bash

check_systemd_user_available() {
	test "$(systemctl --user is-system-running)" = running
}

check_session_dbus_available() {
	busctl --user status >/dev/null 2>&1
}

check_subplot_installed() {
	command -v subplot >/dev/null
}

_get_path() {
	if ! path="$(ctx_get path 2>/dev/null)"
	then
		ctx_set path "$PATH"
		path="$PATH"
	fi
	printf "%s" "$path"
}

install_bin() {
	if ! bindir="$(ctx_get bindir 2>/dev/null)"
	then
		bindir="$(mktemp -d)"
		ctx_set bindir "$bindir"
		ctx_set path "$bindir:$(_get_path)"
	fi
	cmd="$(cap_get cmd)"
	cat >"$bindir/$cmd" <<EOF
#!/bin/sh
exec $(printf "%q/target/debug/%q" "$srcdir" "$cmd") "\$@"
EOF
	chmod +x "$bindir/$cmd"
}

uninstall_bin() {
	rm "$(ctx_get bindir)/$(cap_get cmd)"
}

files_create_from_embedded() {
	files_get "$(cap_get filename)" >"$(cap_get filename)"
}

_run() {
	if PATH="$(_get_path)" "$@" </dev/null >stdout 2>stderr
	then
		ctx_set exit 0
	else
		ctx_set exit "$?"
	fi
	ctx_set stdout "$(cat stdout)"
	ctx_set stderr "$(cat stderr)"
}

run() {
	# TODO: If subplot gains the ability to capture a string as quoted array
	# then use that instead to avoid any eval or extra tools
	read -a args <<<"$(PATH="$(_get_path)" quoted-to-read-escape "$(cap_get cmd)")"
	_run "${args[@]}"
}

setup_subprocess() {
	mkfifo subprocess.pipe
	while cmd="$(cat subprocess.pipe)"
	do
		_run eval "$cmd"
	done &
	ctx_set subprocess_pid "$!"
}

cleanup_subprocess() {
	kill "$(ctx_get subprocess_pid)"
	rm subprocess.pipe
}

subprocess_runs() {
	# TODO: need to synchronise subprocess exit or this is racy
	quoted-to-read-escape "$(cap_get cmd)" >subprocess.pipe
}

subprocess_exits() {
	kill "$(ctx_get subprocess_pid)"
}

command_successful() {
	if [ "$(ctx_get exit)" != 0 ]
	then
		echo "$(ctx_get stderr)" >&2
		return "$(ctx_get exit)"
	fi
}

check_exit_code() {
	assert_eq "$(cap_get exit_code)" "$(ctx_get exit)"
}

stdout_is() {
	assert_eq "$(ctx_get stdout)" "$(cap_get text)"
}

check_runtime_dir_exists() {
	test -e "${XDG_RUNTIME_DIR-/run/user/$(id -u)}"
}

check_runtime_dir_pidfile_match() {
	# TODO: evaluate pattern to fill in pidns and ppid
	# then check if that exists
	return 1
}

check_runtime_dir_pidfile_nomatch() {
	return 1
}

check_path_exists() {
	return 1
}

check_path_locked() {
	return 1
}

check_path_not_locked() {
	return 1
}
